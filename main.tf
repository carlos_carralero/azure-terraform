provider "azurerm" {
  features {}
  subscription_id = var.subid
  tenant_id = var.subtenant
}
#Create resource group
resource "azurerm_resource_group" "terraform_rg" {
  name     = var.resourcegroupname
  location = var.location
}
#Create app service plan
resource "azurerm_app_service_plan" "appserviceplan" {
	name = var.aspname
	location = var.location
	resource_group_name = azurerm_resource_group.terraform_rg.name
	
	sku {
		tier = var.aspskutier
		size = var.aspskusize
	}
}
#Create app service
resource "azurerm_app_service" "appservice" {
	name = var.appservicename
	location = var.location
	resource_group_name = azurerm_resource_group.terraform_rg.name
	app_service_plan_id = azurerm_app_service_plan.appserviceplan.id
	app_settings = {
		"test1" = "www.terraformAzureTest.es",
		"test2" = "other key"
	}
}
#Create app insight for app service
resource "azurerm_application_insights" "appinsight" {
  name                = var.appinsightname
  location            = var.location
  resource_group_name = azurerm_resource_group.terraform_rg.name
  application_type    = var.appinsighttype
}
#Create Storage
resource "azurerm_storage_account" "storage" {
  name                     = var.storagename
  resource_group_name      = azurerm_resource_group.terraform_rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    environment = "staging"
  }
}
#Create Function App
resource "azurerm_function_app" "functionapp" {
  name                       = var.functionappname
  location                   = var.location
  resource_group_name        = azurerm_resource_group.terraform_rg.name
  app_service_plan_id        = azurerm_app_service_plan.appserviceplan.id
  storage_account_name       = azurerm_storage_account.storage.name
  storage_account_access_key = azurerm_storage_account.storage.primary_access_key
}
#Create SQL Server
resource "azurerm_mssql_server" "sqlserver" {
  name                         = var.sqlservername
  resource_group_name          = azurerm_resource_group.terraform_rg.name
  location                     = var.location
  version                      = "12.0"
  administrator_login          = var.sqlServerAdmin
  administrator_login_password = var.sqlServerPW
}
#Addition Firewall Rules
resource "azurerm_sql_firewall_rule" "sqlfirewall" {
  name                = var.sqlfirewallname
  resource_group_name = azurerm_resource_group.terraform_rg.name
  server_name         = azurerm_mssql_server.sqlserver.name
  start_ip_address    = var.startip
  end_ip_address      = var.endip
}
#Create SQL Database
resource "azurerm_mssql_database" "sqldatabase" {
  name           = var.sqldbname
  server_id      = azurerm_mssql_server.sqlserver.id
  collation      = var.sqldbcollation
  license_type   = "LicenseIncluded"
  max_size_gb    = var.sqldbsize
  read_scale     = true
  sku_name       = "BC_Gen5_2"
  zone_redundant = true
}
#Create cosmosdb
resource "azurerm_cosmosdb_account" "cosmosdb" {
  name = var.cosmosdbname
  resource_group_name = azurerm_resource_group.terraform_rg.name
  location = var.location
  offer_type = "Standard"
  #Possible values are GlobalDocument and MongoDB
  kind = var.cosmosdbkind
  #List of IP for selected networks
  ip_range_filter = var.selectedip
  consistency_policy {
    consistency_level = "Session"
  }
  geo_location {
    location          = var.location
    failover_priority = 0
  }
}
#Create alerts
resource "azurerm_monitor_action_group" "main" {
  name                = var.actiongroupname
  resource_group_name = azurerm_resource_group.terraform_rg.name
  short_name          = var.agshortname

  webhook_receiver {
    name        = "callmyapi"
    service_uri = "http://example.com/alert"
  }
}
#Metric alert when transactions are greater than 50 in the storage
resource "azurerm_monitor_metric_alert" "metricalert" {
  name                = var.metricalertname
  resource_group_name = azurerm_resource_group.terraform_rg.name
  scopes              = [azurerm_storage_account.storage.id]
  description         = "Action will be triggered when Transactions count is greater than 50."

  criteria {
    metric_namespace = "Microsoft.Storage/storageAccounts"
    metric_name      = "Transactions"
    aggregation      = "Total"
    operator         = "GreaterThan"
    threshold        = 50

    dimension {
      name     = "ApiName"
      operator = "Include"
      values   = ["*"]
    }
  }

  action {
    action_group_id = azurerm_monitor_action_group.main.id
  }
}