output "sqlserver_admin" {
  value = var.sqlServerAdmin
}
output "sqlserver_pw" {
  value = var.sqlServerPW
}
output "cosmosdb_connection_string" {
  value = azurerm_cosmosdb_account.cosmosdb.connection_strings
}
output "webapp_url" {
  value = azurerm_app_service.appservice.default_site_hostname
}
output "sql_connection_string" {
  value       = "Server=tcp:${azurerm_mssql_server.sqlserver.fully_qualified_domain_name},1433;Initial Catalog=${azurerm_mssql_database.sqldatabase.name};Persist Security Info=False;User ID=${var.sqlServerAdmin};Password=${var.sqlServerPW};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"
}