variable "subid"{
    description = "Subscription ID"
    default = "163ca1a4-4a55-4add-a9ca-fe5aeb9e7ed6"
}
variable "subtenant" {
    description = "Subscription tenant_id"
    default = "386cc92c-0e15-42cf-9183-14ad61539f2d"
}
variable "location" {
    description="Location of resources"
    default="centralus"
}
variable "sqlServerAdmin" {
    description="Admin name for SQL Server"
    default="sqladmin"
}
variable "sqlServerPW" {
    description="Admin's password for SQL Server"
    default="pw1234&A"
}
variable "resourcegroupname" {
    description="Resource group's name"
    default="resourcegrouptftest"
}
variable "aspname" {
    description = "App Service Plan's name"
    default = "appserviceplantftest"
}
variable "aspskutier" {
    description = "App service plan sku's tier"
    default = "free"
}
variable "aspskusize" {
    description = "App service plan sku's size"
    default = "F1"
}
variable "appservicename" {
    description="AppService's name"
    default="appservicetftest"
}
variable "appinsightname" {
    description="AppInsight's name"
    default="appinsighttftest"
}
variable "appinsighttype" {
    description = "App Insight's type"
    default = "web"
}
variable "storagename" {
    description="Storage's name"
    default="storagetftest"
}
variable "functionappname" {
    description = "Function App's name"
    default = "functionapptftest"
}
variable "sqlservername" {
    description="Sql Server's name"
    default="sqlsevertftest"
}
variable "sqlfirewallname" {
    description="SQL Server Firewall's name"
    default="sqlfirewalltftest"
}
variable "startip" {
    description="Start ip"
    default="8.8.8.8"
}
variable "endip" {
    description="End ip"
    default="8.8.8.8"
}
variable "sqldbname" {
    description="Sql DB's name"
    default="sqldbtftest"
}
variable "sqldbcollation" {
    description = "SQL DB's collation"
    default = "SQL_Latin1_General_CP1_CI_AS"
}
variable "sqldbsize" {
    description = "SQL DB's size"
    default = 4
}
variable "cosmosdbname" {
    description="Cosmos DB's name"
    default="cosmosdbtftest"
}
variable "cosmosdbkind" {
    description="Cosmos DB type"
    default="MongoDB"
}
variable "selectedip" {
    description = "List of selected ips separate by ,"
    default = "1.1.1.1,2.2.2.2,3.3.3.3"
}
variable "actiongroupname" {
    description="Action Group's name"
    default="actiongrouptftest"
}
variable "agshortname" {
    description="Action group's short name"
    default="agtftest"
}
variable "metricalertname" {
    description="Metric Alert's name"
    default="metricalerttftest"
}